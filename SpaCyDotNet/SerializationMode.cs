﻿using System;

namespace SpacyDotNet
{
    public enum SerializationMode
    {
        Spacy,
        SpacyAndDotNet,
        DotNet
    }
}
