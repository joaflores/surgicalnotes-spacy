using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;
using SpacyDotNet;
using System.Collections.Generic;
using System.Globalization;

namespace FACSharpPython
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            CliOptions cliOps = new CliOptions();
            cliOps.PathVirtualEnv = "C:\\Users\\Jorge Villacorta\\Documents\\Stuff\\SurgicalNotes\\venv";
            cliOps.Interpreter = "python38.dll";
            PythonRuntimeUtils.Init(cliOps.Interpreter, cliOps.PathVirtualEnv);

            string text = string.Empty;
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            dynamic data = JsonConvert.DeserializeObject(requestBody);

            text = data?.text;

            TextInfo info = new CultureInfo("en-US", false).TextInfo;

            var spacy = new Spacy();

            var nlp = spacy.Load("en_core_web_md");
            var doc = nlp.GetDocument(text);

            var tagged_sent = (from w in doc.Tokens select (w.Text, w.Tag)).ToList();

            var normalizedTextWords = new List<string>();

            List<string> capitalizerTags = new List<string> { "NNP", "NNPS" };
            List<string> noLowerTags = new List<string> { "NN", "NNS" };
            List<string> punctuationTags = new List<string> { "." };

            string previousTag = string.Empty;
            string previousText = string.Empty;

            foreach (var w in tagged_sent)
            {
                string tokenizedWord = w.Text;

                //Check if word should be capitalized

                //exclude words with all letters in upper case
                if (!IsWordUpperCase(w.Text))
                {
                    if (capitalizerTags.Contains(w.Tag, StringComparer.OrdinalIgnoreCase) || previousTag.Equals(".") /*|| previousText.Equals(Environment.NewLine)*/)
                    {
                        tokenizedWord = info.ToTitleCase(w.Text);
                    }
                    else
                    {
                        if (!noLowerTags.Contains(w.Tag, StringComparer.OrdinalIgnoreCase))
                            tokenizedWord = w.Text.ToLower();
                    }
                }

                normalizedTextWords.Add(tokenizedWord);

                previousTag = w.Tag;
                previousText = w.Text;
            }

            //Capitalize first word of the text
            normalizedTextWords[0] = info.ToTitleCase(normalizedTextWords[0]);

            var capitalizedText = string.Join(" ", normalizedTextWords);

            int charIndex = 0;
            char prevCh = '\0';

            string newCapitalizedText = string.Empty;

            foreach (var ch in capitalizedText)
            {
                if (!(prevCh.Equals('\n') && ch.Equals(' ')))
                    newCapitalizedText += ch;

                prevCh = ch;
                charIndex++;
            }

            log.LogInformation("Capitalization finished.");

            return new OkObjectResult(newCapitalizedText);
        }
        private static bool IsWordUpperCase(string word)
        {
            //All upper case
            string upperCaseWord = word.ToUpper();
            return word.Equals(upperCaseWord);
        }
    }
}
