﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FACSharpPython
{
    public class CliOptions
    {
        public string Interpreter { get; set; }
        public string PathVirtualEnv { get; set; }
    }
}
